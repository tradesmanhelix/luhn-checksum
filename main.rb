#!/usr/bin/ruby -w

require File.join(File.dirname(__FILE__), 'luhn_checksum')

luhn_checksum = LuhnChecksum.new

while true
  input = gets.chomp
  puts luhn_checksum.validate(input)
end