# LuhnChecksum - Class for validating whether a given input is
# valid using the Luhn checksum algorithm
class LuhnChecksum
  
  def validate(input)
    cleaned_input = Integer(input) rescue false
    message = validateInput(cleaned_input)
    return "#{input} #{message}"
  end
  
  private
  def validateInput(input)
    message = "is not valid."
    
    if input.is_a? Integer
      sum = 0
      c = 0
      
      # From the rightmost digit, which is the check digit, and moving left
      while input > 0
        # Grab the right-most digit in the given input...
        digit = input % 10
        # ...and then chop it off the initial input
        input /= 10
        
        # For every second digit
        # (start @ 0, so second digit is 1)
        if c % 2 != 0
          # Double the value
          digit *= 2
          
          # If the result of this doubling operation is greater than 9
          if digit > 9
            # Subtract 9 from the product
            digit -= 9
          end
        end
        
        # Sum all the digits as we go
        sum += digit
        
        # Uncomment for debug
        # puts "Digit: #{digit}"
        # puts "Sum: #{sum}"
        
        c += 1
      end
      
      # If the sum modulo 10 is equal to 0 (if the total ends in zero)
      if sum % 10 == 0
        # Then the number is valid according to the Luhn formula
        message = "is valid."
      end
    end
    
    return message
  end
  
end
