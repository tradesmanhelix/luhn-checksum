require File.join(File.dirname(__FILE__), 'luhn_checksum')
require 'test/unit'

class TestLuhnChecksum < Test::Unit::TestCase

  def setup
    @luhn_checksum = LuhnChecksum.new
    @valid_input_message = "is valid."
    @invalid_input_message = "is not valid."
  end

  def test_nil_input
    nil_input = nil
    assert_equal @luhn_checksum.validate(nil_input), "#{nil_input} #{@invalid_input_message}"
  end

  def test_blank_input
    blank_input = ''
    assert_equal @luhn_checksum.validate(blank_input), "#{blank_input} #{@invalid_input_message}"
  end

  def test_nonnumeric_input
    nonnumeric_input = 'ABC'
    assert_equal @luhn_checksum.validate(nonnumeric_input), "#{nonnumeric_input} #{@invalid_input_message}"
  end

  def test_mixed_input
    mixed_input = 'A1B2C3'
    assert_equal @luhn_checksum.validate(mixed_input), "#{mixed_input} #{@invalid_input_message}"
  end

  def test_invalid_input
    invalid_input = 79927398710
    assert_equal @luhn_checksum.validate(invalid_input), "#{invalid_input} #{@invalid_input_message}"
  end

  def test_multiple_invalid_inputs
    file = File.open("test/invalid_luhn_numbers.txt", 'r')
    while !file.eof?
      invalid_input = file.readline.strip
      assert_equal @luhn_checksum.validate(invalid_input), "#{invalid_input} #{@invalid_input_message}"
    end
  end

  def test_valid_input
    valid_input = 79927398713
    assert_equal @luhn_checksum.validate(valid_input), "#{valid_input} #{@valid_input_message}"
  end

  def test_multiple_valid_inputs
    file = File.open("test/valid_luhn_numbers.txt", 'r')
    while !file.eof?
      valid_input = file.readline.strip
      assert_equal @luhn_checksum.validate(valid_input), "#{valid_input} #{@valid_input_message}"
    end
  end
end
