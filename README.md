# Luhn Checksum Algorithm in Ruby
(By Alex Smith @ tradesmanhelix.com)

See this Wikipedia article for more info on the Luhn Checksum Algorithm and its uses:
https://en.wikipedia.org/wiki/Luhn_algorithm

Crated using directions from this gist: https://gist.github.com/tomharris/bc9a942bcd25e029e746

# Usage
The tests can be run using the following command:

    $ ruby luhn_checksum_test.rb

Additionally, an interactive prompt can be accessed via:

    $ ruby main.rb

Some valid inputs to try:
* 4943863806375526
* 5325371974243299

Some invalid inputs to try:
* 4820320811185928
* 4033526378094

Additional valid and invalid inputs can be found in the `test/` directory in this project.